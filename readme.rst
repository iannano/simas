###################
SIMAS FST Univ. Airlangga Surabaya
###################

SIMAS (Sistem Informasi Manajemen Surat)
Tujuan aplikasi ini dibentuk adalah untuk memantau surat masuk dan keluar setiap harinya, baik surat umum ataupun surat untuk pimpinan.

*******************
Dependency
*******************

Dependency pada sistem :

1. Semantic-UI (http://semantic-ui.com/)

2. DataTables (https://www.datatables.net/)

3. jQuery (https://jquery.com/)

Untuk pengembangan secara offline diharapkan mendownload dependency tersebut dan apabila telah selesai diharapkan untuk mengembalikan dependency tersebut menjadi CDN dari link http://cdnjs.com/

**************************
Next Features
**************************
Feature selanjutnya yang akan dikembangkan :

1. Cetak rekap dari surat masuk umum dan pimpinan

2. Perbaikan pada pencarian atau pelacakan surat umum ataupun surat pimpinan

3. Penambahan Otoritas berupa Tata Usaha

4. (Selanjutnya akan menyusul sesuai dengan evaluasi mulai bulan ini hingga akhir desember 2015)