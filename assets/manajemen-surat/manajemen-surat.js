//Global Function
var $base_url = window.location.origin;

function login(a, b){
    $.ajax({
        url: $base_url+'/manajemen_surat/index.php/Check_Validation/login',
        method: 'POST',
        data:{
            username: a,
            password: b
        },
        success: function(response){
            console.log(response);
            var $str = response;
            var $join = $str.split(' ').join('_');
            if(response === "Error"){
                $('#warning_login').removeAttr('hidden');
            }
            else{
                window.location.href= $base_url + "/manajemen_surat/index.php/"+$join+"/Home";
                
            }
        }
    });
}

function simpan_klasifikasi(a, b, c, d){
    $.ajax({
        url: $base_url+"/manajemen_surat/index.php/Super_Admin/Klasifikasi/simpan_data",
        method: 'POST',
        data:{
            idKlasifikasi: a,
            kodeKlasifikasi: b,
            klasifikasi: c,
            uraianKlasifikasi: d
        },
        success: function(response){
            console.log(response);
            window.location.href= $base_url+"/manajemen_surat/index.php/Super_Admin/Klasifikasi";
        }
    });
}

function update_klasifikasi(a, b, c, d){
    $.ajax({
       url: $base_url+"/manajemen_surat/index.php/Super_Admin/Klasifikasi/update_data",
       method: "POST",
       data:{
           idKlasifikasi: a,
           kodeKlasifikasi: b,
           klasifikasi: c,
           uraianKlasifikasi: d
       },
       success: function(response){
           window.location.href= $base_url + "/manajemen_surat/index.php/Super_Admin/Klasifikasi/";
       }
    });
}

function delete_klasifikasi(a){
    $.ajax({
        url: $base_url+"/manajemen_surat/index.php/Super_Admin/Klasifikasi/hapus_data",
        method: 'POST',
        data:{
            idKlasifikasi: $(a).html()
        },
        success: function(response){
            window.location.href=$base_url+"/manajemen_surat/index.php/Super_Admin/Klasifikasi";
        }
    });
}

function pilih_klasifikasi(a){
    $idKlasifikasi = $(a).closest('tr').find('#idKlasifikasi').html();
    $kodeKlasifikiasiDipilih = $(a).closest('tr').find('#kodeKlasifikasi').html();
    $klasifikasiDipilih = $(a).closest('tr').find('#klasifikasi').html();
    $('#klasifikasi_dipilih').html($kodeKlasifikiasiDipilih + " - " + $klasifikasiDipilih);
    $('#val_id_klasifikasi').val($idKlasifikasi);
    $('#btn_show_modal_klasifikasi').addClass('display-none');
    $('#click_able_klasifikasi_dipilih').removeAttr('hidden');
    $('#modal_klasifikasi').modal('hide');
}

function simpan_user(a, b, c, d, e){
    $.ajax({
        url: $base_url+"/manajemen_surat/index.php/Super_Admin/User/simpan_data",
        method: 'POST',
        data:{
            idUser: a,
            username: b,
            password: c,
            namaUser: d,
            idLevel: e
        },
        success: function(response){
            window.location.href= $base_url+"/manajemen_surat/index.php/Super_Admin/User/";
        }
    });
}

function edit_user(a, b){
    
}

function hapus_user(a){
    $.ajax({
        url: $base_url+"/manajemen_surat/index.php/Super_Admin/User/hapus_data",
        method: "POST",
        data:{
            idUser: a
        },
        success: function(response){
            window.location.href=$base_url+"/manajemen_surat/index.php/Super_Admin/User/";
        }
    });
}

function simpan_surat_masuk_dekan(a, b, c, d, e){
    $.ajax({
        url: $base_url+"/manajemen_surat/index.php/Lobby/Surat_Masuk_Dekan/simpan_data",
        method: "POST",
        data:{
            noAgendaSuratMasuk: a,
            noSurat: b,
            asalSurat: c,
            tujuanSurat: d,
            idUser: e,
            idKlasifikasi: "99"
            
        },
        success: function(response){
            window.location.href=$base_url+"/manajemen_surat/index.php/Lobby/Surat_Masuk_Dekan/";
        }
    });
}

function update_surat_masuk_dekan(a, b, c, d, e, f){
    $.ajax({
        url: $base_url+"/manajemen_surat/index.php/Sekretariat/Surat_Masuk_Dekan/simpan_data",
        method: "POST",
        data:{
            idSuratMasuk: a,
            tanggalPembuatanSurat: b,
            idKlasifikasi: c,
            perihalSurat:d,
            diteruskan1: e,
            diteruskan2: f
        },
        success: function(response){
            window.location.href=$base_url+"/manajemen_surat/index.php/Sekretariat/Surat_Masuk_Dekan";
        }
    });
}

function ganti_tahun_arsip_surat_masuk_dekan(a){
    window.location.href= $base_url+"/manajemen_surat/index.php/Lobby/Arsip_Surat_Masuk_Dekan/Tahun/"+a;
}

function ganti_tahun_arsip_surat_masuk_umum(a){
    window.location.href= $base_url+"/manajemen_surat/index.php/Lobby/Arsip_Surat_Masuk_Umum/Tahun/"+a;
}

function simpan_surat_masuk_umum(a, b, c, d, e){
    $.ajax({
        url: $base_url+"/manajemen_surat/index.php/Lobby/Surat_Masuk_Umum/simpan_data",
        method: "POST",
        data:{
            noAgendaSuratMasuk: a,
            asalSuratMasukUmum: b,
            noSuratUmum: c,
            tujuanSuratUmum: d,
            penerimaSurat: e
        },
        success: function(response){
            window.location.href= $base_url+"/manajemen_surat/index.php/Lobby/Surat_Masuk_Umum/";
        }
    });
}

function hapus_surat_masuk(a){
    $.ajax({
        url: $base_url+"/manajemen_surat/index.php/T_Surat_Masuk/hapus_data/"+a,
        method: "POST",
        success: function(response){
            window.location.href= $base_url + "/manajemen_surat/index.php/T_Surat_Masuk/";
        }
    });
}

function show_modal_klasifikasi(){
    $('#modal_klasifikasi').modal('show');
}

function simpan_surat_keluar(a, b, c, d, e, f){
    $.ajax({
        url: $base_url+"/manajemen_surat/index.php/Sub_Bagian/Surat_Keluar/simpan_data",
        method: "POST",
        data:{
            noAgendaSurat: a,
            tujuanSurat: b,
            tanggalSuratKeluar: c,
            perihalSurat: d,
            idKlasifikasi: e,
            pemroses: f
        },
        success: function(response){
            window.location.href= $base_url+"/manajemen_surat/index.php/Sub_Bagian/List_Surat_Keluar/";
        }
    });
}