<html>
    <head>
        <title>.::Manajemen Surat::.</title>
    </head>
    <body>
        <div class="ui segment custom-banner">
            <center>
                <h3>Manajemen Surat Fakultas FST Universitas Airlangga</h3>
            </center>
        </div>
        <div class="ui middle aligned center aligned grid">
            <div class="column">
                <div class="ui raised segment transparent-background">
                    <h4>Fakultas Sains dan Teknologi</h4>
                    <h5>Jalan Mulyorejo Kampus C UNAIR</h5>
                </div>
                <div class="ui horizontal divider header">
                    <h4 class="blue text">Login Admin</h4>
                </div>
                <div class="ui raised segment transparent-background">
                    <form class="ui form" id="form_login">
                        <div class="field">
                            <div class="ui left icon input fluid">
                                <i class="users icon"></i>
                                <input type="text" placeholder="Username" id="username">
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui left icon input fluid">
                                <i class="lock icon"></i>
                                <input type="password" placeholder="Password" id="password">
                            </div>
                        </div>
                        <div class="field">
                            <button type="button" class="ui blue button fluid" onclick="login(username.value, password.value)">Login</button>
                        </div>
                    </form>
                </div>
                <span>Lupa password anda ? Klik <a href="#">disini</a></span>
                <div class="ui inverted red segment" id="warning_login" hidden>
                    <p>Maaf username dan password anda tidak terdaftar pada database kami.</p>
                </div>
            </div>
        </div>
    </body>
</html>