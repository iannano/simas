<div class="container">
    <div class="ui raised segment custom-banner">
        List Surat Keluar
    </div>
</div>
<div class="ui raised segment">
    <table class="ui table celled">
        <thead>
            <tr>
                <th>No. Urut Agenda</th>
                <th>No. Surat</th>
                <th>Tujuan Surat</th>
                <th>Tanggal Surat</th>
                <th>Perihal</th>
                <th>Klasifikasi</th>
                <th>Pemroses</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list as $data) { ?>
            <tr>
                <td><?php echo $data->noUrutAgenda; ?></td>
                <td><?php echo $data->noSuratKeluar; ?></td>
                <td><?php echo $data->tujuanSurat; ?></td>
                <td><?php echo $data->tanggalSurat; ?></td>
                <td><?php echo $data->perihalSurat; ?></td>
                <td><?php echo $data->klasifikasi;?></td>
                <td><?php echo $data->pemrosesSurat; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>