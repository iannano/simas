<div class="container">
    <div class="ui raised segment custom-banner">
        Buat Surat Keluar
    </div>
</div>
<div class="ui raised segment">
    <form class="ui form">
        <div class="ui left aligned container">
            <div class="field">
                <label>No. Agenda</label>
                <input type="text" name="no_agenda_surat_keluar" id="val_no_agenda" placeholder="No Agenda Surat Keluar">
            </div>
            <div class="field">
                <label>Tujuan / Kepada</label>
                <input type="text" name="tujuan_surat_keluar" id="val_tujuan_surat_keluar" placeholder="Tujuan Surat">
            </div>
            <div class="field">
                <label>Tanggal Surat</label>
                <input type="date" name="tanggal_surat" id="val_tanggal_surat">
            </div>
            <div class="field">
                <label>Perihal Surat</label>
                <textarea name="perihal_surat_keluar" placeholder="Perihal Surat Keluar" id="val_perihal_surat_keluar"></textarea>
            </div>'
            <div class="field">
                <label>Kode Surat</label>
                <button type="button" class="mini ui blue button" onclick="show_modal_klasifikasi()" id="btn_show_modal_klasifikasi">Pilih Klasifikasi</button>
                <a href="#" id="click_able_klasifikasi_dipilih" onclick="show_modal_klasifikasi()" hidden><span id="klasifikasi_dipilih"></span></a>
                <input type="text" name="idKlasifikasi" id="val_id_klasifikasi" hidden>
                <br>
                <span style="font-size: smaller; color: red;">(*)Untuk merubah klasifikasi yang dipilih. Klik tulisan di atas.</span>
            </div>
            <div class="field">
                <label>Pemroses</label>
                <input type="text" name="pemroses" id="val_pemroses" placeholder="Pemroses Surat">
            </div>
            <button type="button" class="ui blue button" onclick="simpan_surat_keluar(val_no_agenda.value, val_tujuan_surat_keluar.value, val_tanggal_surat.value, val_perihal_surat_keluar.value, val_id_klasifikasi.value, val_pemroses.value)"><i class="save icon"></i> Simpan</button>
            <button type="button" class="ui red button"><i class="reply icon"></i> Kembali</button>
        </div>
    </form>
</div>
<div class="ui modal" id="modal_klasifikasi">
    <i class="close icon"></i>
    <div class="header">
        Pilih Klasifikasi Surat
    </div>
    <div class="content">
        <table class="ui table celled" id="table_klasifikasi">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Kode Klasifikasi</th>
                    <th>Klasifikasi</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($klasifikasi as $data){?>
                <tr>
                    <td id="idKlasifikasi"><?php echo $data->idKlasifikasi; ?></td>
                    <td id="kodeKlasifikasi"><?php echo $data->kodeKlasifikasi; ?></td>
                    <td id="klasifikasi"><?php echo $data->klasifikasi; ?></td>
                    <td><button type="button" class="mini ui blue button" onclick="pilih_klasifikasi(this)"><i class="check icon"></i> Pilih</button></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <button type="button" class="ui red button"><i class="remove icon"></i> Tutup</button>
    </div>
</div>