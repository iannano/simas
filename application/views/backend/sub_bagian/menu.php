<html>
    <head>
        <meta charset="UTF-8">
        <title>.::Manajemen Surat::.</title>
    </head>
    <body>
        <div class="ui menu custom-banner">
            <div class="header item white-color">
                Manajemen Surat
            </div>
            <a class="item white-color" href="<?php echo base_url()?>index.php/Sub_Bagian/Home">
                <i class="home icon"></i>Beranda
            </a>
            <div class="ui dropdown item white-color">
                <i class="external share icon"></i> Surat Keluar <i class="dropdown icon"></i>
                <div class="menu">
                    <a class="item" href="<?php echo base_url(); ?>index.php/Sub_Bagian/Surat_Keluar">Buat Surat Keluar</a>
                    <a class="item" href="<?php echo base_url(); ?>index.php/Sub_Bagian/List_Surat_Keluar">List Surat Keluar</a>
                    <a class="item" href="<?php echo base_url(); ?>index.php/Sub_Bagian/No_Surat_Keluar">Nomer Surat Yang Tersedia</a>
                </div>
            </div>
            <div class="right item">
                <a class="white-color" href="<?php echo base_url()?>index.php/Check_Validation/logout"><i class="user icon"></i> Log Out</a>
            </div>
        </div>
        <div class="ui container">
                <div class="ui raised segment transparent-background">
                    <h4>Fakultas Sains dan Teknologi</h4>
                    <h5>Kampus C Universitas Airlangga Surabaya</h5>
                </div>