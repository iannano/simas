<div class="container">
    <div class="ui raised segment custom-banner">
        <h3>Edit Data Klasifikasi Baru</h3>
    </div>
</div>
<div class="ui raised segment">
    <form class="ui form">
        <div class="ui left aligned container">
            <?php foreach ($list as $data){?>
            <div class="field">
                <label>ID</label>
                <input type="text" name="id" id="val_id" value="<?php echo $data->idKlasifikasi; ?>" readonly>
            </div>
            <div class="field">
                <label>Kode Klasifikasi</label>
                <input type="text" name="kode_klasifikasi" id="val_kode_klasifikasi" placeholder="Kode Klasifikasi" value="<?php echo $data->kodeKlasifikasi; ?>"> 
            </div>
            <div class="field">
                <label>Klasifikasi</label>
                <input type="text" name="klasifikasi" id="val_klasifikasi" placeholder="Klasifikasi" value="<?php echo $data->klasifikasi; ?>">
            </div>
            <div class="field">
                <label>Uraian Klasifikasi</label>
                <textarea name="uraian_klasifikasi" id="val_uraian_klasifikasi" placeholder="Uraian Klasifikasi" value="<?php echo $data->uraianKlasifikasi?>"></textarea>
            </div>
            <?php } ?>
            <button type="button" class="ui blue button" onclick="update_klasifikasi(val_id.value, val_kode_klasifikasi.value, val_klasifikasi.value, val_uraian_klasifikasi.value)"><i class="save icon"></i> Update</button>
            <a href="<?php echo base_url(); ?>index.php/Super_Admin/Klasifikasi/"><button type="button" class="ui red button"><i class="reply icon"></i> Kembali</button></a>
        </div>
    </form>
</div>