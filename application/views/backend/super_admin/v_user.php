<div class="container">
    <div class="ui menu custom-banner">
        <div class="header item white-color">
            Daftar User
        </div>
        <a class="item white-color" href="<?php echo base_url()?>index.php/Super_Admin/User/Form_Tambah"><i class="plus icon"></i> Tambah Data</a>
    </div>
</div>
<div class="ui raised segment">
    <table class="ui table celled">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nama Lengkap</th>
                <th>Hak Akses</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list as $data){?>
            <tr>
                <td><?php echo $data->idUser; ?></td>
                <td><?php echo $data->namaUser; ?></td>
                <td><?php echo $data->level; ?></td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/Super_Admin/User/Form_Edit/<?php echo $data->idUser; ?>"><button type="button" class="mini ui green button"><i class="pencil icon"></i> Edit</button></a>
                    <a href="<?php echo base_url(); ?>index.php/Super_Admin/User/Form_Hapus/<?php echo $data->idUser; ?>"<button type="button" class="mini ui red button"><i class="trash icon"></i> Delete</button>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>