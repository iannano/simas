<div class="container">
    <div class="ui menu custom-banner">
        <div class="header item white-color">
            Master Klasifikasi
        </div>
        <a class="item white-color" href="<?php echo base_url(); ?>index.php/Super_Admin/Klasifikasi/form_tambah"><i class="plus icon"></i> Tambah Data</a>
    </div> 
</div>
<div class="ui raised segment">
    <table class="ui table celled" id="table_klasifikasi">
        <thead>
            <tr>
                <th>No.</th>
                <th>Kode Klasifikasi</th>
                <th>Klasifikasi</th>
                <th>Uraian Klasifikasi</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list as $data){ ?>
            <tr>
                <td><?php echo $data->idKlasifikasi; ?></td>
                <td><?php echo $data->kodeKlasifikasi; ?></td>
                <td><?php echo $data->klasifikasi; ?></td>
                <td><?php echo $data->uraianKlasifikasi; ?></td>
                <td>
                    <a href="<?php echo base_url();?>index.php/Super_Admin/Klasifikasi/form_edit/<?php echo $data->idKlasifikasi;?>"><button type="button" class="mini ui green button"><i class="pencil icon"></i> Edit</button> </a>
                    <a href="<?php echo base_url();?>index.php/Super_Admin/Klasifikasi/form_hapus/<?php echo $data->idKlasifikasi; ?>"><button type="button" class="mini ui red button"><i class="trash icon"></i> Hapus</button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>