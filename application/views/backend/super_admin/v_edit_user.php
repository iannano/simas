<div class="container">
    <div class="ui raised segment custom-banner">
        <h3>Edit User</h3>
    </div>
</div>
<div class="ui raised segment">
    <form class="ui form">
        <div class="ui left aligned container">
            <?php foreach ($list as $data){ ?>
            <div class="field">
                <label>No. User</label>
                <input type="text" name="no_user" id="val_no_user" value="<?php echo $data->idUser; ?>" readonly>
            </div>
            <div class="field">
                <label>Nama User</label>
                <input type="text" name="nama_user" id="val_nama_user" placeholder="Nama Lengkap User" value="<?php echo $data->namaUser; ?>">
            </div>
            <?php } ?>
            <div class="field">
                <label>Hak Akses</label>
                <select name="hak_akses" id="val_hak_akses">
                    <?php foreach ($hak_akses as $data){?>
                    <option value="<?php echo $data->idLevel; ?>"><?php echo $data->Level; ?></option>
                    <?php } ?>
                </select>
            </div>
            <button type="button" class="ui blue button" onclick="simpan_user(val_no_user.value, val_username.value, val_password.value, val_nama_user.value, val_hak_akses.value)"><i class="save icon"></i> Simpan</button>
            <a href="<?php echo base_url()?>index.php/Super_Admin/User/"><button type="button" class="ui red button"><i class="reply icon"></i> Kembali</button></a>
            
        </div>
    </form>
</div>