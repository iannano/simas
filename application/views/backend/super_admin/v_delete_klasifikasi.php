<div class="container">
    <div class="ui raised segment custom-banner">
        Konfirmasi Mengahapus Data
    </div>
</div>
<div class="ui raised segment">
    <b>Apakah anda yakin ingin menghapus data :</b>
    <?php foreach ($list as $data){ ?>
    <table>
        <tr>
            <td>ID Klasifikasi</td>
            <td>&nbsp; &nbsp; : &nbsp; &nbsp;</td>
            <td id="idKlasifikasi"><?php echo $data->idKlasifikasi; ?></td>
        </tr>
        <tr>
            <td>Kode Klasifikasi</td>
            <td>&nbsp; &nbsp; : &nbsp; &nbsp;</td>
            <td><?php echo $data->kodeKlasifikasi; ?></td>
        </tr>
        <tr>
            <td>Klasifikasi</td>
            <td>&nbsp; &nbsp; : &nbsp; &nbsp;</td>
            <td><?php echo $data->klasifikasi; ?></td>
        </tr>
        <tr>
            <td>Uraian Klasifikasi</td>
            <td>&nbsp; &nbsp; : &nbsp; &nbsp;</td>
            <td><?php echo $data->uraianKlasifikasi; ?></td>
        </tr>
    <?php }?>
    </table>
    <button type="button" class="ui blue button" onclick="delete_klasifikasi(idKlasifikasi)"><i class="check icon"></i> Iya</button>
    <button type="button" class="ui red button"><i class="cancel icon"></i> Tidak</button>
</div>