<div class="container">
    <div class="ui raised segment custom-banner">
        <h3>Hapus Data User</h3>
    </div>
</div>
<div class="ui raised segment">
    <form class="ui form">
        <div class="ui left aligned container">
            <?php foreach ($list as $data) { ?>
            <div class="field">
                <label>No. User</label>
                <input type="text" name="no_user" id="val_no_user" value="<?php echo $data->idUser?>" readonly>
            </div>
            <div class="field">
                <label>Nama User</label>
                <input type="text" name="nama_user" id="val_nama_user" value="<?php echo $data->namaUser; ?>" readonly>
            </div>
            <button type="button" class="ui blue button" onclick="hapus_user(val_no_user.value)"><i class="trash icon"></i> Hapus</button>
            <a href="<?php echo base_url()?>index.php/Super_Admin/User/"><button type="button" class="ui red button"><i class="reply icon"></i> Kembali</button></a>
            <?php } ?>
        </div>
    </form>
</div>