<html>
    <head>
        <meta charset="UTF-8">
        <title>.::Manajemen Surat::.</title>
    </head>
    <body>
        <div class="ui menu custom-banner">
            <div class="header item white-color">
                Manajemen Surat
            </div>
            <a class="item white-color" href="<?php echo base_url()?>index.php/Super_Admin/Home">
                <i class="home icon"></i>Beranda
            </a>
            <div class="ui dropdown item white-color">
                <i class="list layout icon"></i>Data Master <i class="dropdown icon"></i>
                <div class="menu">
                    <a class="item" href="<?php echo base_url();?>index.php/Super_Admin/Klasifikasi">Klasifikasi Surat</a>
                    <a class="item" href="<?php echo base_url();?>index.php/Super_Admin/User">Users</a>
                </div>
            </div>
            <div class="right item">
                <a class="white-color" href="<?php echo base_url()?>index.php/Check_Validation/logout"><i class="user icon"></i> Log Out</a>
            </div>
        </div>
        <div class="ui container">
                <div class="ui raised segment transparent-background">
                    <h4>Fakultas Sains dan Teknologi</h4>
                    <h5>Kampus C Universitas Airlangga Surabaya</h5>
                </div>