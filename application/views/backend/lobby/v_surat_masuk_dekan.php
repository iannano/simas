<div class="container">
    <div class="ui menu custom-banner">
        <div class="header item white-color">
            Surat Masuk Pimpinan
        </div>
        <a class="item white-color" href="<?php echo base_url(); ?>index.php/Lobby/Surat_Masuk_Dekan/form_tambah"><i class="plus icon"></i> Tambah Data</a>
    </div> 
</div>
<div class="ui raised segment">
    Surat Masuk Hari Ini. Tanggal <?php echo $tanggal; ?>
</div>
<div class="ui raised segment">
    <table class="ui table celled" id="table_klasifikasi">
        <thead>
            <tr>
                <th>No. Agenda</th>
                <th>No. Surat</th>
                <th>Asal Surat</th>
                <th>Tujuan Surat</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list as $data){ ?>
            <tr>
                <td><?php echo $data->noAgendaSuratMasuk; ?></td>
                <td><?php echo $data->noSurat; ?></td>
                <td><?php echo $data->asalSurat; ?></td>
                <td><?php echo $data->tujuanSurat; ?></td>
                <td>
                    <a href="<?php echo base_url();?>index.php/Super_Admin/Surat_Masuk_Dekan/form_edit/<?php echo $data->idSuratMasuk;?>"><button type="button" class="mini ui green button"><i class="pencil icon"></i> Edit</button> </a>
                    <a href="<?php echo base_url();?>index.php/Super_Admin/Surat_Masuk_Dekan/form_delete/<?php echo $data->idSuratMasuk ?>"><button type="button" class="mini ui red button"><i class="trash icon"></i> Hapus</button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>