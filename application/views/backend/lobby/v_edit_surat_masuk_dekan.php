<div class="container">
    <div class="ui raised segment custom-banner">
        Edit Masuk Untuk Pimpinan
    </div>
</div>
<div class="ui raised segment">
    <form class="ui form">
        <div class="ui left aligned container">
            <div class="field">
                <label>No. Agenda</label>
                <input type="text" name="noAgendaSUrat" id="val_no_agenda_surat" value="<?php echo $no_agenda ?>" readonly>
            </div>
            <div class="field">
                <label>Asal Surat</label>
                <input type="text" name="asalSurat" id="val_asal_surat">
            </div>
            <div class="field">
                <label>No. Surat</label>
                <input type="text" name="noSuratMasuk" id="val_no_surat">
            </div>
            <div class="field">
                <label>Tujuan Surat</label>
                <input type="text" name="tujuanSurat" id="val_tujuan_surat">
            </div>
            <div class="field">
                <label>Penerima Di Lobby</label>
                <input type="text" name="idPenerimaLobby" id="val_id_user" value="<?php echo $idUser ?>" hidden>
                <input type="text" name="penerimaLobby" value="<?php echo $session; ?>" readonly>
            </div>
            <button type="button" class="ui blue button" onclick="simpan_surat_masuk_dekan(val_no_agenda_surat.value, val_no_surat.value, val_asal_surat.value, val_tujuan_surat.value, val_id_user.value)"><i class="save icon"></i> Simpan</button>
            <a href="<?php echo base_url(); ?>index.php/Lobby/Surat_Masuk_Dekan"><button type="button" class="ui red button"><i class="reply icon"></i> Kembali</button></a>
        </div>
    </form>
</div>