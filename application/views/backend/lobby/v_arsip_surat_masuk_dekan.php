<div class="container">
    <div class="ui raised segment custom-banner">
        Arsip Surat Masuk Dekan Tahun <?php echo $tahun_saat_ini; ?>
    </div>
</div>
<div class="ui raised segment">
    <form class="ui form">
        <div class="ui left aligned container">
            <div class="field">
                <label>Tahun Arsip</label>
                <select name="tahun_arsip_surat_masuk_dekan" onchange="ganti_tahun_arsip_surat_masuk_dekan(this.value)">
                    <option value="">-- Pilih Tahun Arsip --</option>
                    <?php foreach ($tahun as $data) { ?>
                    <option value="<?php echo $data->tahun; ?>"><?php echo $data->tahun; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="ui right aligned container">
                <button type="button" class="ui blue button"><i class="print icon"></i> Cetak Arsip</button>
            </div>
        </div>
    </form>
</div>
<div class="ui raised segment">
    <table class="ui table celled" id="table_klasifikasi">
        <thead>
            <tr>
                <th>No. Agenda</th>
                <th>Tanggal Terima Surat</th>
                <th>No. Surat</th>
                <th>Asal Surat</th>
                <th>Tujuan Surat</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list as $data){ ?>
            <tr>
                <td><?php echo $data->noAgendaSuratMasuk; ?></td>
                <td><?php echo $data->tanggalPenerimaanSurat; ?></td>
                <td><?php echo $data->noSurat; ?></td>
                <td><?php echo $data->asalSurat; ?></td>
                <td><?php echo $data->tujuanSurat; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>