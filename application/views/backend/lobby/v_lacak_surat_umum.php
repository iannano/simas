<div class="container">
    <div class="ui menu custom-banner">
        <div class="header item white-color">
            Lacak Surat Masuk Umum
        </div>
    </div>
</div>
<div class="ui raised segment">
    <table class="ui table celled" id="table_surat_masuk_umum">
        <thead>
            <tr>
                <th>No. Agenda</th>
                <th>Asal Surat</th>
                <th>No. Surat</th>
                <th>Tujuan Surat</th>
                <th>Penerima Surat</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list as $data){ ?>
            <tr>
                <td><?php echo $data->noAgendaSuratMasukUmum; ?></td>                
                <td><?php echo $data->asalSuratMasukUmum; ?></td>
                <td><?php echo $data->noSuratUmum; ?></td>
                <td><?php echo $data->tujuanSuratUmum; ?></td>
                <td><?php echo $data->penerimaSurat; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>