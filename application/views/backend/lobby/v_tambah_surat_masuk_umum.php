<div class="container">
    <div class="ui raised segment custom-banner">
        Tambah Surat Masuk Umum
    </div>
</div>
<div class="ui raised segment">
    <form class="ui form">
        <div class="ui left aligned container">
            <div class="field">
                <label>No. Agenda</label>
                <input type="text" name="no_agenda" id="val_no_agenda" value="<?php echo $no_agenda; ?>">
            </div>
            <div class="field">
                <label>Asal Surat</label>
                <input type="text" name="asal_surat" id="val_asal_surat" placeholder="Asal Surat">
            </div>
            <div class="field">
                <label>No. Surat</label>
                <input type="text" name="no_surat" id="val_no_surat" placeholder="No. Surat atau Perihal Surat">
            </div>
            <div class="field">
                <label>Tujuan Surat</label>
                <input type="text" name="tujuan_surat" id="val_tujuan_surat" placeholder="Tujuan Surat">
            </div>
            <div class="field">
                <label>Penerima Surat</label>
                <input type="text" name="penerima_surat" id="val_penerima_surat" placeholder="Nama Penerima">
            </div>
            <button type="button" class="ui blue button" onclick="simpan_surat_masuk_umum(val_no_agenda.value, val_asal_surat.value, val_no_surat.value, val_tujuan_surat.value, val_penerima_surat.value)"><i class="save icon"></i> Simpan</button>
            <button type="button" class="ui red button"><i class="reply icon"></i> Kembali</button>
        </div>
    </form>
</div>