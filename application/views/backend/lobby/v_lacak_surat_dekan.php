<div class="container">
    <div class="ui raised segment custom-banner">
        <h3>Lacak Surat</h3>
    </div>
</div>
<div class="ui raised segment">
    <table class="ui table celled">
        <thead>
            <tr>
                <th>No. Agenda</th>
                <th>No. Surat</th>
                <th>Asal Surat</th>
                <th>Tujuan Surat</th>
                <th>Posisi Terakhir Surat</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list as $data){ ?>
            <tr>
                <td><?php echo $data->noAgendaSuratMasuk; ?></td>
                <td><?php echo $data->noSurat; ?></td>
                <td><?php echo $data->asalSurat; ?></td>
                <td><?php echo $data->tujuanSurat; ?></td>
                <td>
                    <?php 
                    if($data->perihalSurat == ""){
                        echo "Surat Masih Berada Di Loby";
                    }
                    else{
                        if($data->perihalSurat != "" && $data->diteruskan1 == ""){
                            echo "Surat Berada Pada Sekretariat";
                        }
                        else{
                            if($data->diteruskan1!="" || $data->diteruskan2 !=""){
                                echo "Surat Berada Pada Kabag dan Kasubag";
                            }
                        }
                    }
                    ?>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>