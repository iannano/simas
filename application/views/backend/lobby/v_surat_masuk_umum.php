<div class="container">
    <div class="ui menu custom-banner">
        <div class="header item white-color">
            Surat Masuk Umum
        </div>
        <a class="item white-color" href="<?php echo base_url(); ?>index.php/Lobby/Surat_Masuk_Umum/form_tambah"><i class="plus icon"></i> Tambah Data</a>
    </div>
</div>
<div class="ui raised segment">
    <p>Surat Masuk Umum Pada Bulan <?php echo $bulan; ?> Tahun <?php echo $tahun; ?></p>
</div>
<div class="ui raised segment">
    <table class="ui table celled" id="table_surat_masuk_umum">
        <thead>
            <tr>
                <th>No. Agenda</th>
                <th>No. Surat</th>
                <th>Asal Surat</th>
                <th>Tujuan Surat</th>
                <th>Penerima Surat</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list as $data){ ?>
            <tr>
                <td><?php echo $data->noAgendaSuratMasukUmum; ?></td>
                <td><?php echo $data->noSuratUmum; ?></td>
                <td><?php echo $data->asalSuratMasukUmum; ?></td>
                <td><?php echo $data->tujuanSuratUmum; ?></td>
                <td><?php echo $data->penerimaSurat; ?></td>
                <td>
                    <button type="button" class="mini ui blue button"><i class="pencil icon"></i> Edit</button>
                    <button type="button" class="mini ui red button"><i class="trash icon"></i> Hapus</button>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>