<div class="container">
    <div class="ui segment custom-banner">
        Surat Yang Berada Pada Lobby
    </div>
</div>
<div class="ui raised segment">
    <table class="ui table celled" id="table_surat_lobby">
        <thead>
            <tr>
                <th>No. Agenda Surat</th>
                <th>No. Surat</th>
                <th>Asal Surat</th>
                <th>Tujuan Surat</th>
                <th>Tanggal Surat</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list as $data){ ?>
            <tr>
                <td><?php echo $data->noAgendaSuratMasuk; ?></td>
                <td><?php echo $data->noSurat; ?></td>
                <td><?php echo $data->asalSurat; ?></td>
                <td><?php echo $data->tujuanSurat; ?></td>
                <td><?php echo $data->tanggalPenerimaanSurat; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>