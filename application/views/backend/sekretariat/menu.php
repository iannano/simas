<html>
    <head>
        <meta charset="UTF-8">
        <title>.::Manajemen Surat::.</title>
    </head>
    <body>
        <div class="ui menu custom-banner">
            <div class="header item white-color">
                Manajemen Surat
            </div>
            <a class="item white-color" href="<?php echo base_url()?>index.php/Sekretariat/Home">
                <i class="home icon"></i>Beranda
            </a>
            <a class="item white-color" href="<?php echo base_url();?>index.php/Sekretariat/Surat_Masuk_Dekan"><i class="mail icon"></i> Surat Masuk Pimpinan</a>
            <a class="item white-color" href="<?php echo base_url();?>index.php/Sekretariat/Arsip_Surat_Diteruskan">
                <i class="list layout icon"></i> Arsip Surat Diteruskan
            </a>
            <a class="item white-color" href="<?php echo base_url() ?>index.php/Sekretariat/Check_Surat_Lobby/"><i class="find icon"></i> Cek Surat Lobby</a>
            <div class="right item">
                <a class="white-color" href="<?php echo base_url()?>index.php/Check_Validation/logout"><i class="user icon"></i> Log Out</a>
            </div>
        </div>
        <div class="ui container">
                <div class="ui raised segment transparent-background">
                    <h4>Fakultas Sains dan Teknologi</h4>
                    <h5>Kampus C Universitas Airlangga Surabaya</h5>
                </div>