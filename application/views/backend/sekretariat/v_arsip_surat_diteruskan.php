<div class="container">
    <div class="ui raised segment">
        Arsip Surat Diteruskan
    </div>
</div>
<div class="ui raised segment">
    <table class="ui table celled">
        <thead>
            <tr>
                <th>No.</th>
                <th>No. Surat</th>
                <th>Asal Surat</th>
                <th>Tujuan Surat</th>
                <th>Klasifikasi</th>
                <th>Perihal</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list as $data) {?>
            <tr>
                <td><?php echo $data->noAgendaSuratMasuk; ?></td>
                <td><?php echo $data->noSurat; ?></td>
                <td><?php echo $data->asalSurat; ?></td>
                <td><?php echo $data->tujuanSurat; ?></td>
                <td><?php echo $data->klasifikasi; ?></td>
                <td><?php echo $data->perihalSurat; ?></td>
                <td><button type="button" class="mini ui blue button"><i class="pencil icon"></i> Edit</button></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>