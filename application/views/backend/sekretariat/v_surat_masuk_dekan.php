<div class="container">
    <div class="ui menu custom-banner">
        <div class="header item white-color">
            Surat Masuk Dekan
        </div>
    </div> 
</div>
<div class="ui raised segment">
    <table class="ui table celled" id="table_klasifikasi">
        <thead>
            <tr>
                <th>No. Agenda</th>
                <th>No. Surat</th>
                <th>Asal Surat</th>
                <th>Tujuan Surat</th>
                <th>Tanggal Penerimaan</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($list as $data){ ?>
            <tr>
                <td><?php echo $data->noAgendaSuratMasuk; ?></td>
                <td><?php echo $data->noSurat; ?></td>
                <td><?php echo $data->asalSurat; ?></td>
                <td><?php echo $data->tujuanSurat; ?></td>
                <td><?php echo $data->tanggalPenerimaanSurat;?></td>
                <td>
                    <a href="<?php echo base_url();?>index.php/Sekretariat/Surat_Masuk_Dekan/form_teruskan/<?php echo $data->idSuratMasuk; ?>"><button type="button" class="mini ui green button"><i class="forward icon"></i> Diteruskan</button> </a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>