<div class="container">
    <div class="ui raised segment custom-banner">
        Surat Masuk Dekan Diteruskan
    </div>
</div>
<div class="ui raised segment">
    <form class="ui form">
        <?php foreach($list as $data){ ?>
        <div class="ui grid">
            <div class="eight wide column">
                <div class="ui left aligned container">
                    <div class="field">
                        <label>Tanggal Penerimaan Surat</label>
                        <input type="text" name="tanggal_penerimaan" id="val_tanggal_penerimaan" value="<?php echo $data->tanggalPenerimaanSurat; ?>" readonly>
                    </div>
                    <div class="field">
                        <label>No. Agenda Surat</label>
                        <input type="text" name="no_surat" id="val_id_surat_masuk" value="<?php echo $data->idSuratMasuk; ?>" hidden>
                        <input type="text" name="no_agenda_surat" value="<?php echo $data->noAgendaSuratMasuk; ?>" readonly>
                    </div>
                    <div class="field">
                        <label>No. Surat</label>
                        <input type="text" name="no_surat" id="val_no_surat" value="<?php echo $data->noSurat; ?>" readonly>
                    </div>
                    <div class="field">
                        <label>Asal Surat</label>
                        <input type="text" name="asal_surat" id="val_asal_surat" value="<?php echo $data->asalSurat; ?>" readonly>
                    </div>
                    <div class="field">
                        <label>Tujuan Surat</label>
                        <input type="text" name="tujuan_surat" id="val_tujuan_surat" value="<?php echo $data->tujuanSurat; ?>" readonly>
                    </div>
                </div>
            </div>
            <div class="eight wide column">
                <div class="ui left aligned container">
                    <div class="field">
                        <label>Tanggal Pembuatan Surat</label>
                        <input type="date" name="tanggal_pembuatan_surat" id="val_tanggal_pembuatan_surat">
                    </div>
                    <div class="field">
                        <label>Perihal Surat</label>
                        <textarea name="perihal_surat" id="val_perihal_surat" placeholder="Perihal Surat"></textarea>
                    </div>
                    <div class="field">
                        <label>Klasifikasi Surat</label>
                        <button type="button" class="mini ui blue button" onclick="show_modal_klasifikasi()" id="btn_show_modal_klasifikasi">Pilih Klasifikasi</button>
                        <a href="#" id="click_able_klasifikasi_dipilih" onclick="show_modal_klasifikasi()" hidden><span id="klasifikasi_dipilih"></span></a>
                        <input type="text" name="idKlasifikasi" id="val_id_klasifikasi" hidden>
                        <br>
                        <span style="font-size: smaller; color: red;">(*)Untuk merubah klasifikasi yang dipilih. Klik tulisan di atas.</span>
                    </div>
                    <div class="field">
                        <label>Diteruskan 1</label>
                        <input type="text" name="diteruskan1" id="val_diteruskan1">
                    </div>
                    <div class="field">
                        <label>Diteruskan 2</label>
                        <input type="text" name="diteruskan2" id="val_diteruskan2">
                    </div>
                </div>
            </div>
        </div>
        <div class="ui left aligned container">
            <button type="button" class="ui blue button" onclick="update_surat_masuk_dekan(val_id_surat_masuk.value, val_tanggal_pembuatan_surat.value, val_id_klasifikasi.value, val_perihal_surat.value, val_diteruskan1.value, val_diteruskan2.value)"><i class="forward icon"></i> Teruskan Surat</button>
            <a href="<?php echo base_url(); ?>index.php/Sekretariat/Surat_Masuk_Dekan/"><button type="button" class="ui red button"><i class="reply icon"></i> Kembali</button></a>
        </div>
        <?php } ?>
    </form>
</div>
<div class="ui modal" id="modal_klasifikasi">
    <i class="close icon"></i>
    <div class="header">
        Pilih Klasifikasi Surat
    </div>
    <div class="content">
        <table class="ui table celled" id="table_klasifikasi">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Kode Klasifikasi</th>
                    <th>Klasifikasi</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($klasifikasi as $data){?>
                <tr>
                    <td id="idKlasifikasi"><?php echo $data->idKlasifikasi; ?></td>
                    <td id="kodeKlasifikasi"><?php echo $data->kodeKlasifikasi; ?></td>
                    <td id="klasifikasi"><?php echo $data->klasifikasi; ?></td>
                    <td><button type="button" class="mini ui blue button" onclick="pilih_klasifikasi(this)"><i class="check icon"></i> Pilih</button></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <button type="button" class="ui red button"><i class="remove icon"></i> Tutup</button>
    </div>
</div>