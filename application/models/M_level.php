<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_level
 *
 * @author ian-nano
 */
class M_level extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    
    function get_all_data(){
        $data = $this->db->get('Level');
        return $data->result();
    }
    
    function get_all_by_user($a){
        $query = $this->db->query("(select l.idLevel, l.Level from User u
                            inner join Level l on l.idLevel = u.idLevel
                            where idUser = '".$a."'
                            )
                            union
                            (
                            select idLevel, Level from Level
                            where idLevel not in (select l.idLevel from User u
                            inner join Level l on l.idLevel = u.idLevel and idUser ='".$a."')
                        )");
        return $query->result();
    }
}
