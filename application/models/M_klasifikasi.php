<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_Klasifikasi
 *
 * @author ian-nano
 */
class M_klasifikasi extends CI_Model {
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    function insert($a){
        $this->db->insert('Klasifikasi', $a);
    }
    
    function update($a){
        $this->db->where('idKlasifikasi', $a['idKlasifikasi']);
        $this->db->update('Klasifikasi', $a['dataKlasifikasi']);
    }
    
    function delete($a){
        $this->db->where('idKlasifikasi', $a['idKlasifikasi']);
        $this->db->delete('Klasifikasi');
    }
    
    function get_all_data(){
        $data = $this->db->get('Klasifikasi');
        return $data->result();
    }
    
    function get_data_by_id($a){
        $this->db->where('idKlasifikasi', $a);
        $this->db->from('Klasifikasi');
        $data = $this->db->get();
        return $data->result();
    }
    
    function generate_id(){
        $this->db->select('*');
        $this->db->from('Klasifikasi');
        $count = $this->db->count_all_results();
        $id = $count + 1;
        return $id;
    }
}
