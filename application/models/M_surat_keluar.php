<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_surat_keluar
 *
 * @author ian-nano
 */
class M_surat_keluar extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    
    function get_all_data(){
        $this->db->select('*');
        $this->db->from('SuratKeluar');
        $this->db->join('Klasifikasi','SuratKeluar.idKlasifikasi = Klasifikasi.idKlasifikasi');
        $data = $this->db->get();
        return $data->result();
    }
    
//    function generate_id($a){
//        $this->db->get('SuratKeluar');
//        $count = $this->db->count_all_results();
//        if($count != 0){
//            $data = $this->db->query("Select noUrutAgenda, tanggalSurat from SuratKeluar
//                                group by noUrutAgenda desc limit 1");
//            $ret = $data->row();
//            $tanggal = $ret->tanggalSurat;
//            $noAgenda = $ret->noUrutAgenda;
//            if($tanggal != $a){
//                $noAgenda = $noAgenda + 5;
//                return $noAgenda;
//            }
//            else{
//                $noAgenda = $noAgenda + 1;
//                return $noAgenda;
//            }
//        }
//        else{
//            if($count == 0){
//                return 1;
//            }
//        }
//    }
    
    function simpan($a){
        $this->db->insert('SuratKeluar', $a);
    }
}
