<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_T_Surat_Masuk
 *
 * @author ian-nano
 */
class M_T_Surat_Masuk extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }
    
    function simpan_data($a){
        $this->db->insert('t_surat_masuk', $a);
    }
    
    function count_data(){
        $this->db->select('*');
        $this->db->from('t_surat_masuk');
        return $this->db->count_all_results();
    }
    
    function delete_data($a){
        $this->db->where('no_agenda', $a);
        $this->db->delete('t_surat_masuk');
    }
    
    function get_all_data(){
        $data = $this->db->get('t_surat_masuk');
        return $data->result();
    }
    
    function get_by_id($a){
        $this->db->where('no_agenda', $a);
        $this->db->from('t_surat_masuk');
        $query = $this->db->get();
        return $query->result();
    }
}
