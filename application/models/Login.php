<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author Ian-Nano-PC
 */
class Login extends CI_Model {
    
    function __construct() {
        parent::__construct();
        $this->load->helper('security');
    }
    
    function check_admin($json){
        $decode = json_decode($json, true);
        $username = $decode['username'];
        $password = $decode['password'];
        $str = do_hash($password, 'md5');
        $this->db->where('username', $username);
        $this->db->where('password', $str);
        $this->db->from('User');
        $result = $this->db->count_all_results();
        if($result == 1){
            $this->db->select('idUser,namaUser,level');
            $this->db->from('User');
            $this->db->join('Level','User.idLevel = Level.idLevel');
            $this->db->where('username', $username);
            $data = $this->db->get();
            $ret = $data->row();
            $this->session->set_userdata('idUser', $ret->idUser);
            $this->session->set_userdata('username', $ret->namaUser);
            echo $ret->level;
        }
        else{
            echo "Error";
        }
    }
}
