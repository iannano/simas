<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_surat_masuk_dekan
 *
 * @author ian-nano
 */
class M_surat_masuk_dekan extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    
    function simpan($a){
        $this->db->insert('SuratMasukDekan', $a);
    }
    
    function update($a){
        $this->db->where('idSuratMasuk',$a['idSuratMasuk']);
        $this->db->update('SuratMasukDekan', $a['update_data']);
    }
    
    function delete(){
        
    }
    
    function get_all_data(){
        $tanggal = date('Y-m-d');
        $this->db->select('*');
        $this->db->from('SuratMasukDekan');
        $this->db->where('tanggalPenerimaanSurat', $tanggal);
        $data = $this->db->get();
        return $data->result();
    }
    
    function get_data_by_id($a){
        $this->db->select('*');
        $this->db->from('SuratMasukDekan');
        $this->db->where('idSuratMasuk', $a);
        $data = $this->db->get();
        return $data->result();
    }
    
    function check_surat_telah_diteruskan(){
        $this->db->select('idSuratMasuk, noAgendaSuratMasuk, noSurat, asalSurat, tujuanSurat, klasifikasi, perihalSurat');
        $this->db->from('SuratMasukDekan');
        $this->db->join('Klasifikasi','SuratMasukDekan.idKlasifikasi = Klasifikasi.idKlasifikasi');
        $this->db->where('perihalSurat !=', "");
        $data = $this->db->get();
        return $data->result();
    }
    
    function check_surat_lobby(){
        $this->db->select('*');
        $this->db->from('SuratMasukDekan');
        $this->db->where('perihalSurat', "");
        $data = $this->db->get();
        return $data->result();
    }
    
    function lacak_surat(){
        $this->db->select('*');
        $this->db->from('SuratMasukDekan');
        $data = $this->db->get();
        return $data->result();
    }
    
    function generate_no_agenda(){
        $this->db->from('SuratMasukDekan');
        $this->db->where('tanggalPenerimaanSurat', date('Y-m-d'));
        $count = $this->db->count_all_results();
        $no_agenda = $count+1;
        return $no_agenda;
    }
    
    function check_arsip_surat_masuk_dekan(){
        $data = $this->db->query("SELECT * FROM SuratMasukDekan
                            WHERE year(tanggalPenerimaanSurat) = year(now())");
        return $data->result();
    }
    
    function tahun_arsip(){
        $data = $this->db->query("SELECT year(tanggalPenerimaanSurat) as tahun 
                                    from SuratMasukDekan
                                    group by tahun desc");
        return $data->result();
    }
    
    function get_data_by_tahun($a, $b){
        $data = $this->db->query("SELECT * FROM SuratMasukDekan
                            WHERE year(tanggalPenerimaanSurat) = $a or tanggalPenerimaanSurat < $b");
        return $data->result();
    }
}
