<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_user
 *
 * @author ian-nano
 */
class M_user extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    
    function insert($a){
        $this->db->insert('User', $a);
    }
    
    function update(){
        
    }
    
    function delete($a){
        $this->db->where('idUser', $a);
        $this->db->delete('User');
    }
    
    function get_all_data(){
        $this->db->select('idUser, namaUser, level');
        $this->db->from('User');
        $this->db->join('Level','Level.idLevel=User.IdLevel');
        $data = $this->db->get();
        return $data->result();
    }
    
    function get_user_by_id($a){
        $this->db->where('idUser', $a);
        $this->db->from('User');
        $data = $this->db->get();
        return $data->result();
    }
    
    function count_data(){
        $this->db->select('*');
        $this->db->from('User');
        $count = $this->db->count_all_results();
        $data = $count + 1;
        return $data;
    }
}
