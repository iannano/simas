<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of M_surat_masuk_umum
 *
 * @author ian-nano
 */
class M_surat_masuk_umum extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    
    function simpan($a){
        $this->db->insert('SuratMasukUmum', $a);
    }
    
    function get_all_data(){
        $this->db->select('*');
        $this->db->from('SuratMasukUmum');
        $data = $this->db->get();
        return $data->result();
    }
    
    function generate_id(){
        $date = date('Y');
        $this->db->select('*');
        $this->db->from('SuratMasukUmum');
        $this->db->where('year(tanggalSuratMasukUmum)', $date);
        $count = $this->db->count_all_results();
        $id = $count + 1;
        return $id;
    }
    
    function check_arsip_surat_masuk_dekan(){
        $data = $this->db->query("SELECT * FROM SuratMasukUmum
                            WHERE year(tanggalSuratMasukUmum) = year(now())");
        return $data->result();
    }
    
    function tahun_arsip(){
        $data = $this->db->query("SELECT year(tanggalSuratMasukUmum) as tahun 
                                    from SuratMasukUmum
                                    group by tahun desc");
        return $data->result();
    }
    
    function get_data_by_tahun($a, $b){
        $data = $this->db->query("SELECT * FROM SuratMasukUmum
                            WHERE year(tanggalSuratMasukUmum) = $a or tanggalSuratMasukUmum < $b");
        return $data->result();
    }
}
