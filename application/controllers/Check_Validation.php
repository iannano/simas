<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Check_Validation
 *
 * @author Ian-Nano-PC
 */
//if(isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')){
    class Check_Validation extends CI_Controller {
        
        function __construct() {
            parent::__construct();
            $this->load->model('login');
        }

        function login(){
            $data = array(
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password')
            );
            $json = json_encode($data);
            $this->login->check_admin($json);
        }
        
        function logout(){
            $this->session->sess_destroy();
            redirect(base_url());
        }
    }
//} 
//else {
    
//}
