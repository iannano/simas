<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Klasifikasi
 *
 * @author ian-nano
 */
class Klasifikasi extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->view('libs/b_css');
        $this->load->view('libs/b_script');
        $this->load->model('m_klasifikasi');
    }
    
    function index(){
        $data['list'] = $this->m_klasifikasi->get_all_data();
        $this->load->view('backend/super_admin/menu');
        $this->load->view('backend/super_admin/v_klasifikasi', $data);
        $this->load->view('backend/super_admin/footer');
    }
    
    function form_tambah(){
        $data['id'] = $this->m_klasifikasi->generate_id();
        $this->load->view('backend/super_admin/menu');
        $this->load->view('backend/super_admin/v_tambah_klasifikasi', $data);
        $this->load->view('backend/super_admin/footer');
    }
    
    function form_edit($a){
        $data['list'] = $this->m_klasifikasi->get_data_by_id($a);
        $this->load->view('backend/super_admin/menu');
        $this->load->view('backend/super_admin/v_edit_klasifikasi', $data);
        $this->load->view('backend/super_admin/footer');
    }
    
    function form_hapus($a){
        $data['list'] = $this->m_klasifikasi->get_data_by_id($a);
        $this->load->view('backend/super_admin/menu');
        $this->load->view('backend/super_admin/v_delete_klasifikasi', $data);
        $this->load->view('backend/super_admin/footer');
    }
    
    function simpan_data(){
        $data = array(
            'idKlasifikasi' => $this->input->post('idKlasifikasi'),
            'kodeKlasifikasi' => $this->input->post('kodeKlasifikasi'),
            'klasifikasi' => $this->input->post('klasifikasi'),
            'uraianKlasifikasi' => $this->input->post('uraianKlasifikasi')
        );
        $this->m_klasifikasi->insert($data);
    }
    
    function update_data(){
        $data['idKlasifikasi'] = $this->input->post('idKlasifikasi');
        $data['dataKlasifikasi'] = array(
            'kodeKlasifikasi' => $this->input->post('kodeKlasifikasi'),
            'klasifikasi' => $this->input->post('klasifikasi'),
            'uraianKlasifikasi' => $this->input->post('uraianKlasifikasi')
        );
        $this->m_klasifikasi->update($data);
    }
    
    function hapus_data(){
        $data['idKlasifikasi'] = $this->input->post('idKlasifikasi');
        $this->m_klasifikasi->delete($data);
    }
}
