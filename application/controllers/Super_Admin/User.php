<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author ian-nano
 */
class User extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->view('libs/b_css');
        $this->load->view('libs/b_script');
        $this->load->model('m_user');
        $this->load->model('m_level');
        $this->load->helper('security');
    }
    
    function index(){
        $data['list'] = $this->m_user->get_all_data();
        $this->load->view('backend/super_admin/menu');
        $this->load->view('backend/super_admin/v_user', $data);
        $this->load->view('backend/super_admin/footer');
    }
    
    function form_tambah(){
        $data['hak_akses'] = $this->m_level->get_all_data();
        $data['count'] = $this->m_user->count_data();
        $this->load->view('backend/super_admin/menu');
        $this->load->view('backend/super_admin/v_tambah_user', $data);
        $this->load->view('backend/super_admin/footer');
    }
    
    function form_edit($a){
        $data['list'] = $this->m_user->get_user_by_id($a);
        $data['hak_akses'] = $this->m_level->get_all_by_user($a);
        $this->load->view('backend/super_admin/menu');
        $this->load->view('backend/super_admin/v_edit_user', $data);
        $this->load->view('backend/super_admin/footer');
    }
    
    function form_hapus($a){
        $data['list'] = $this->m_user->get_user_by_id($a);
        $this->load->view('backend/super_admin/menu');
        $this->load->view('backend/super_admin/v_hapus_user', $data);
        $this->load->view('backend/super_admin/footer');
    }
    
    function simpan_data(){
        $hash_code = do_hash($this->input->post('password'),"md5");
        $data = array(
            'idUser' => $this->input->post('idUser'),
            'username' => $this->input->post('username'),
            'password' => $hash_code,
            'namaUser' => $this->input->post('namaUser'),
            'idLevel' => $this->input->post('idLevel')
        );
        $this->m_user->insert($data);
    }
    
    function update_data(){
        
    }
    
    function hapus_data(){
        $this->m_user->delete($this->input->post('idUser'));
    }
}
