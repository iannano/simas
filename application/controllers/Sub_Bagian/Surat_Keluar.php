<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Surat_Keluar
 *
 * @author ian-nano
 */
class Surat_Keluar extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->view('libs/b_css');
        $this->load->view('libs/b_script');
        $this->load->model('m_klasifikasi');
        $this->load->model('m_surat_keluar');
    }
    
    function index(){
        $tanggal = date('Y-m-d');
        //$data['noUrutAgenda'] = $this->m_surat_keluar->generate_id($tanggal);
        $data['klasifikasi'] = $this->m_klasifikasi->get_all_data();
        $this->load->view('backend/sub_bagian/menu');
        $this->load->view('backend/sub_bagian/v_surat_keluar', $data);
        $this->load->view('backend/sub_bagian/footer');
    }
    
    function simpan_data(){
        $tahun_saat_ini  = date('Y');
        $data = array(
            'idSuratKeluar' => "",
            'noUrutAgenda' => $this->input->post('noAgendaSurat'),
            'noSuratKeluar' => $this->input->post('noAgendaSurat')."/U.N.3.1.8/".$this->input->post('idKlasifikasi')."/".$tahun_saat_ini,
            'tujuanSurat' => $this->input->post('tujuanSurat'),
            'tanggalSurat' => $this->input->post('tanggalSuratKeluar'),
            'perihalSurat' => $this->input->post('perihalSurat'),
            'pemrosesSurat' => $this->input->post('pemroses'),
            'idKlasifikasi' => $this->input->post('idKlasifikasi')
        );
        $this->m_surat_keluar->simpan($data);
    }
}
