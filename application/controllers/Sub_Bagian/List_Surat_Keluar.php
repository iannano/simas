<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of List_Surat_Keluar
 *
 * @author ian-nano
 */
class List_Surat_Keluar extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->view('libs/b_css');
        $this->load->view('libs/b_script');
        $this->load->model('m_surat_keluar');
    }
    
    function index(){
        $data['list'] = $this->m_surat_keluar->get_all_data();
        $this->load->view('backend/sub_bagian/menu');
        $this->load->view('backend/sub_bagian/v_list_surat_keluar', $data);
        $this->load->view('backend/sub_bagian/footer');
    }
}
