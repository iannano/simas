<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Home
 *
 * @author Ian-Nano-PC
 */
class Home extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->view('libs/f_css');
        $this->load->view('libs/f_script');
        $this->load->model('login');
    }
    function index(){
        $this->load->view('home');   
    }
} 
