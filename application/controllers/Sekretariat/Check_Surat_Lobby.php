<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Check_Surat_Lobby
 *
 * @author ian-nano
 */
class Check_Surat_Lobby extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->view('libs/b_css');
        $this->load->view('libs/b_script');
        $this->load->model('m_surat_masuk_dekan');
    }
    
    function index(){
        $data['list'] = $this->m_surat_masuk_dekan->check_surat_lobby();
        $this->load->view('backend/sekretariat/menu');
        $this->load->view('backend/sekretariat/v_check_surat_lobby', $data);
        $this->load->view('backend/sekretariat/footer');
    }
}
