<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Surat_Masuk_Dekan
 *
 * @author ian-nano
 */
class Surat_Masuk_Dekan extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->view('libs/b_css');
        $this->load->view('libs/b_script');
        $this->load->model('m_surat_masuk_dekan');
        $this->load->model('m_klasifikasi');
    }
    
    function index(){
        $data['list'] = $this->m_surat_masuk_dekan->check_surat_lobby();
        $this->load->view('backend/sekretariat/menu');
        $this->load->view('backend/sekretariat/v_surat_masuk_dekan', $data);
        $this->load->view('backend/sekretariat/footer');
    }
    
    function form_teruskan($a){
        $data['list'] = $this->m_surat_masuk_dekan->get_data_by_id($a);
        $data['klasifikasi'] = $this->m_klasifikasi->get_all_data();
        $this->load->view('backend/sekretariat/menu');
        $this->load->view('backend/sekretariat/v_surat_masuk_dekan_diteruskan', $data);
        $this->load->view('backend/sekretariat/footer');
    }
    
    function simpan_data(){
        $data['idSuratMasuk'] = $this->input->post('idSuratMasuk');
        $data['update_data'] = array(
            'perihalSurat' => $this->input->post('perihalSurat'),
            'tanggalPembuatanSurat' => $this->input->post('tanggalPembuatanSurat'),
            'diteruskan1' => $this->input->post('diteruskan1'),
            'diteruskan2' => $this->input->post('diteruskan2'),
            'idKlasifikasi' => $this->input->post('idKlasifikasi')
        );
        $this->m_surat_masuk_dekan->update($data);
    }
}
