<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arsip_Surat_Masuk_Dekan
 *
 * @author ian-nano
 */
class Arsip_Surat_Masuk_Dekan extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->view('libs/b_css');
        $this->load->view('libs/b_script');
        $this->load->model('m_surat_masuk_dekan');
    }
    
    function index(){
        $data['tahun_saat_ini'] = date('Y');
        $data['list'] = $this->m_surat_masuk_dekan->check_arsip_surat_masuk_dekan();
        $data['tahun'] = $this->m_surat_masuk_dekan->tahun_arsip();
        $this->load->view('backend/lobby/menu');
        $this->load->view('backend/lobby/v_arsip_surat_masuk_dekan', $data);
        $this->load->view('backend/lobby/footer');
        
    }
    
    function tahun($a){
        $data['tahun_saat_ini'] = $a;
        $tanggal=date('Y-m-d');
        $data['list'] = $this->m_surat_masuk_dekan->get_data_by_tahun($a, $tanggal);
        $data['tahun'] = $this->m_surat_masuk_dekan->tahun_arsip();
        $this->load->view('backend/lobby/menu');
        $this->load->view('backend/lobby/v_arsip_surat_masuk_dekan', $data);
        $this->load->view('backend/lobby/footer');
    }
}
