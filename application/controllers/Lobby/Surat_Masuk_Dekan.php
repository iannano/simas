<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Surat_Masuk_Dekan
 *
 * @author ian-nano
 */
class Surat_Masuk_Dekan extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->view('libs/b_css');
        $this->load->view('libs/b_script');
        $this->load->model('m_surat_masuk_dekan');
    }
    
    function index(){
        $data['tanggal'] = date('d-m-Y');
        $data['list'] = $this->m_surat_masuk_dekan->get_all_data();
        $this->load->view('backend/lobby/menu');
        $this->load->view('backend/lobby/v_surat_masuk_dekan', $data);
        $this->load->view('backend/lobby/footer');
    }
    
    function form_tambah(){
        $data['no_agenda'] = $this->m_surat_masuk_dekan->generate_no_agenda();
        $data['session'] = $this->session->userdata('username');
        $data['idUser'] = $this->session->userdata('idUser');
        $this->load->view('backend/lobby/menu');
        $this->load->view('backend/lobby/v_tambah_surat_masuk_dekan', $data);
        $this->load->view('backend/lobby/footer');
    }
    
    function form_edit(){
        
    }
    
    function simpan_data(){
        $tanggalPenerimaanSurat = date('Y-m-d');
        $data = array(
            'idSuratMasuk' => '',
            'noAgendaSuratMasuk' => $this->input->post('noAgendaSuratMasuk'),
            'noSurat' => $this->input->post('noSurat'),
            'asalSurat' => $this->input->post('asalSurat'),
            'tujuanSurat' => $this->input->post('tujuanSurat'),
            'tanggalPenerimaanSurat' => $tanggalPenerimaanSurat,
            'tanggalPembuatanSurat' => '',
            'perihalSurat' => '',
            'diteruskan1' => '',
            'diteruskan2' => '',
            'idUser' => $this->input->post('idUser'),
            'idKlasifikasi' => $this->input->post('idKlasifikasi')
        );
        $this->m_surat_masuk_dekan->simpan($data);
    }
}
