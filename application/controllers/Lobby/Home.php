<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Home
 *
 * @author ian-nano
 */
class Home extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->view('libs/b_css');
        $this->load->view('libs/b_script');
    }
    
    function index(){
        $this->load->view('backend/lobby/menu');
        $this->load->view('backend/lobby/footer');
    }
}
