<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Lacak_Surat_Umum
 *
 * @author ian-nano
 */
class Lacak_Surat_Umum extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->view('libs/b_css');
        $this->load->view('libs/b_script');
        $this->load->model('m_surat_masuk_umum');
    }
    
    function index(){
        $data['list'] = $this->m_surat_masuk_umum->get_all_data();
        $this->load->view('backend/lobby/menu');
        $this->load->view('backend/lobby/v_lacak_surat_umum', $data);
        $this->load->view('backend/lobby/footer');
    }
}
