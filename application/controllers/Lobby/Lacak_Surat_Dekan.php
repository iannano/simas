<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Lacak Surat
 *
 * @author ian-nano
 */
class Lacak_Surat_Dekan extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->view('libs/b_css');
        $this->load->view('libs/b_script');
        $this->load->model('m_surat_masuk_dekan');
    }
    
    function index(){
        $data['list'] = $this->m_surat_masuk_dekan->lacak_surat();
        $this->load->view('backend/lobby/menu');
        $this->load->view('backend/lobby/v_lacak_surat_dekan', $data);
        $this->load->view('backend/lobby/footer');
    }
}
