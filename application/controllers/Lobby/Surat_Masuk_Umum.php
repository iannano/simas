<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Surat_Masuk_Umum
 *
 * @author ian-nano
 */
class Surat_Masuk_Umum extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->view('libs/b_css');
        $this->load->view('libs/b_script');
        $this->load->model('m_surat_masuk_umum');
    }
    
    function index(){
        $data['bulan'] = date('m');
        $data['tahun'] = date('Y');
        $data['list'] = $this->m_surat_masuk_umum->get_all_data();
        $this->load->view('backend/lobby/menu');
        $this->load->view('backend/lobby/v_surat_masuk_umum', $data);
        $this->load->view('backend/lobby/footer');
    }
    
    function simpan_data(){
        $tanggal = date('Y-m-d');
        $data = array(
            'noAgendaSuratMasukUmum' => $this->input->post('noAgendaSuratMasuk'),
            'tanggalSuratMasukUmum' => $tanggal,
            'asalSuratMasukUmum' => $this->input->post('asalSuratMasukUmum'),
            'noSuratUmum' => $this->input->post('noSuratUmum'),
            'tujuanSuratUmum' => $this->input->post('tujuanSuratUmum'),
            'penerimaSurat' => $this->input->post('penerimaSurat')
        );
        $this->m_surat_masuk_umum->simpan($data);
    }
    
    function form_tambah(){
        $data['no_agenda'] = $this->m_surat_masuk_umum->generate_id();
        $data['session'] = $this->session->userdata('username');
        $this->load->view('backend/lobby/menu');
        $this->load->view('backend/lobby/v_tambah_surat_masuk_umum', $data);
        $this->load->view('backend/lobby/footer');
    }
}
